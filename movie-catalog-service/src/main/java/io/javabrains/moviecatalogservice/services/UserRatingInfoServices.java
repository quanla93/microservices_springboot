package io.javabrains.moviecatalogservice.services;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import io.javabrains.moviecatalogservice.models.Rating;
import io.javabrains.moviecatalogservice.models.UserRating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.Arrays;

@Service
public class UserRatingInfoServices {

    @Autowired
    WebClient.Builder webClientBuilder;

    @HystrixCommand(
            fallbackMethod = "getFallBackUserRating",
            threadPoolKey = "UserRatingInfoPool",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value = "20"),
                    @HystrixProperty(name = "MaxQueueSize", value = "10")
            }
    )
    public UserRating getUserRating(final String userId) {
        // with restTemplate
//restTemplate.getForObject("http://localhost:8083/ratingsdata/user/"+ userId, UserRating.class);

        UserRating ratings = webClientBuilder.build()
                .get()
                .uri("http://ratings-data-service/ratingsdata/user/" + userId)
                .retrieve()
                .bodyToMono(UserRating.class)
                .timeout(Duration.ofSeconds(3))
                .block();
        return ratings;
    }

    private UserRating getFallBackUserRating(final String userId){
        UserRating userRating = new UserRating();
        userRating.setUserId(userId);
        userRating.setRatings(Arrays.asList(new Rating("0", 0)));
        return userRating;
    }

}

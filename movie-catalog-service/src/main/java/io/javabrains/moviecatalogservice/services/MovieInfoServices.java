package io.javabrains.moviecatalogservice.services;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import io.javabrains.moviecatalogservice.models.CatalogItem;
import io.javabrains.moviecatalogservice.models.Movie;
import io.javabrains.moviecatalogservice.models.Rating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;

@Service
public class MovieInfoServices {

    @Autowired
    WebClient.Builder webClientBuilder;

    @HystrixCommand(
            fallbackMethod = "getFallBackMovieInfo",
            threadPoolKey = "MovieInfoPool",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value = "20"),
                    @HystrixProperty(name = "MaxQueueSize", value = "10")
            }
    )
    public CatalogItem getMovieInfo(final Rating ra) {
        Movie movie = webClientBuilder
                .build()
                .get()
                .uri("http://movie-info-service/movies/" + ra.getMovieId())
                .retrieve()
                .bodyToMono(Movie.class)
                .timeout(Duration.ofSeconds(3))
                .block();
//restTemplate.getForObject("http://localhost:8082/movies/" + ra.getMovieId(), Movie.class);
        return new CatalogItem(movie.getName(), movie.getDescription(), ra.getRating());
    }

    private CatalogItem getFallBackMovieInfo(final Rating ra){
        return new CatalogItem("null", "null", 0);
    }
}
